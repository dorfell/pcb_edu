EESchema Schematic File Version 4
LIBS:pcb_edu-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title "EDU_PCB-SRAM Memory"
Date ""
Rev ""
Comp "Universidad Nacional de Colombia"
Comment1 "Dorfell Parra"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Memory_RAM:CY7C199 U?
U 1 1 5CCC3F3D
P 5350 3850
F 0 "U?" H 5675 5050 50  0000 C CNN
F 1 "CY7C199" H 5750 2700 50  0000 C CNN
F 2 "" H 5350 3850 50  0001 C CNN
F 3 "" H 5350 3850 50  0001 C CNN
	1    5350 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 3050 4400 3050
Text Label 4675 3050 2    50   ~ 0
MEM_A1
Wire Wire Line
	4750 3150 4400 3150
Text Label 4675 3150 2    50   ~ 0
MEM_A2
Wire Wire Line
	4750 2950 4400 2950
Text Label 4675 2950 2    50   ~ 0
MEM_A0
Wire Wire Line
	4750 3350 4400 3350
Text Label 4675 3350 2    50   ~ 0
MEM_A4
Wire Wire Line
	4750 3450 4400 3450
Text Label 4675 3450 2    50   ~ 0
MEM_A5
Wire Wire Line
	4750 3250 4400 3250
Text Label 4675 3250 2    50   ~ 0
MEM_A3
Wire Wire Line
	4750 3650 4400 3650
Text Label 4675 3650 2    50   ~ 0
MEM_A7
Wire Wire Line
	4750 3750 4400 3750
Text Label 4675 3750 2    50   ~ 0
MEM_A8
Wire Wire Line
	4750 3550 4400 3550
Text Label 4675 3550 2    50   ~ 0
MEM_A6
Wire Wire Line
	4750 3950 4400 3950
Text Label 4725 3950 2    50   ~ 0
MEM_A10
Wire Wire Line
	4750 4050 4400 4050
Text Label 4725 4050 2    50   ~ 0
MEM_A11
Wire Wire Line
	4750 3850 4400 3850
Text Label 4675 3850 2    50   ~ 0
MEM_A9
Wire Wire Line
	5950 3050 6300 3050
Text Label 6000 3050 0    50   ~ 0
MEM_D1
Wire Wire Line
	5950 3150 6300 3150
Text Label 6000 3150 0    50   ~ 0
MEM_D2
Wire Wire Line
	5950 2950 6300 2950
Text Label 6000 2950 0    50   ~ 0
MEM_D0
Wire Wire Line
	5950 3350 6300 3350
Text Label 6000 3350 0    50   ~ 0
MEM_D4
Wire Wire Line
	5950 3450 6300 3450
Text Label 6000 3450 0    50   ~ 0
MEM_D5
Wire Wire Line
	5950 3250 6300 3250
Text Label 6000 3250 0    50   ~ 0
MEM_D3
Wire Wire Line
	5950 3650 6300 3650
Text Label 6000 3650 0    50   ~ 0
MEM_D7
Wire Wire Line
	4750 4550 4400 4550
Text Label 4675 4550 2    50   ~ 0
MEM_CE
Wire Wire Line
	5950 3550 6300 3550
Text Label 6000 3550 0    50   ~ 0
MEM_D6
Wire Wire Line
	4750 4250 4400 4250
Text Label 4725 4250 2    50   ~ 0
MEM_A13
Wire Wire Line
	4750 4350 4400 4350
Text Label 4725 4350 2    50   ~ 0
MEM_A14
Wire Wire Line
	4750 4150 4400 4150
Text Label 4725 4150 2    50   ~ 0
MEM_A12
Wire Wire Line
	4750 4650 4400 4650
Text Label 4675 4650 2    50   ~ 0
MEM_OE
Wire Wire Line
	4750 4750 4400 4750
Text Label 4675 4750 2    50   ~ 0
MEM_WE
$Comp
L Device:C_Small C?
U 1 1 5CD0A6DD
P 3550 3850
AR Path="/5CB37600/5CD0A6DD" Ref="C?"  Part="1" 
AR Path="/5CB37616/5CD0A6DD" Ref="C?"  Part="1" 
F 0 "C?" H 3450 3925 50  0000 L CNN
F 1 "100nF" V 3650 3750 50  0000 L CNN
F 2 "" H 3550 3850 50  0001 C CNN
F 3 "~" H 3550 3850 50  0001 C CNN
	1    3550 3850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5CD0A70B
P 3550 4325
AR Path="/5CB37600/5CD0A70B" Ref="#PWR?"  Part="1" 
AR Path="/5CB37616/5CD0A70B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3550 4075 50  0001 C CNN
F 1 "GND" H 3555 4152 50  0000 C CNN
F 2 "" H 3550 4325 50  0001 C CNN
F 3 "" H 3550 4325 50  0001 C CNN
	1    3550 4325
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5CD0A712
P 3550 3275
AR Path="/5CB37600/5CD0A712" Ref="#PWR?"  Part="1" 
AR Path="/5CB37616/5CD0A712" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3550 3125 50  0001 C CNN
F 1 "+3.3V" H 3565 3448 50  0000 C CNN
F 2 "" H 3550 3275 50  0001 C CNN
F 3 "" H 3550 3275 50  0001 C CNN
	1    3550 3275
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 3950 3550 4325
Wire Wire Line
	3550 3275 3550 3750
$Comp
L power:+3.3V #PWR?
U 1 1 5CD0C620
P 5350 2475
AR Path="/5CB37600/5CD0C620" Ref="#PWR?"  Part="1" 
AR Path="/5CB37616/5CD0C620" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5350 2325 50  0001 C CNN
F 1 "+3.3V" H 5365 2648 50  0000 C CNN
F 2 "" H 5350 2475 50  0001 C CNN
F 3 "" H 5350 2475 50  0001 C CNN
	1    5350 2475
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5CD0C667
P 5350 5250
AR Path="/5CB37600/5CD0C667" Ref="#PWR?"  Part="1" 
AR Path="/5CB37616/5CD0C667" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5350 5000 50  0001 C CNN
F 1 "GND" H 5355 5077 50  0000 C CNN
F 2 "" H 5350 5250 50  0001 C CNN
F 3 "" H 5350 5250 50  0001 C CNN
	1    5350 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 2475 5350 2650
Wire Wire Line
	5350 5050 5350 5250
Text Notes 2575 2025 0    118  ~ 24
SRAM Memory
Wire Notes Line
	2450 5825 7750 5825
Wire Notes Line
	7750 5825 7750 1625
Wire Notes Line
	7750 1625 2450 1625
Wire Notes Line
	2450 1625 2450 5825
$EndSCHEMATC
